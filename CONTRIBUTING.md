# How to Contribute

*Gitlab project: https://gitlab.com/dhbmarcos/strl*

To contribute with this project, you can use, and publicize.
If you find a error, inconsistency or improvement, you can open a new issue or contribute with a open issue. Go to https://gitlab.com/dhbmarcos/strl/-/issues for check or open issues.

**In advance, thanks for yours contributions.**
