# ![](logo/strl.svg) STRL - Simplest Technology Readiness Level

The Simplest Technology Readiness Level classifies the things in 3 levels:

1. ![](undefined.svg)
2. ![](unstable.svg)
3. ![](stable.svg)
