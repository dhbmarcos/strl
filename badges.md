# Badges

Get link below to use STRL badges in your project.

## Undefined
### SVG
- Image: ![](undefined.svg)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/undefined.svg

### PNG
- Image: ![](undefined.png)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/undefined.png

## Unstable
### SVG
- Image: ![](unstable.svg)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/unstable.svg

### PNG
- Image: ![](unstable.png)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/unstable.png

## Stable
### SVG
- Image: ![](stable.svg)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/stable.svg

### PNG
- Image: ![](stable.png)
- Link: https://gitlab.com/dhbmarcos/strl/-/raw/v0.7.1/stable.png
