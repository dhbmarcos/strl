# Images

## SVG
![](strl.svg)

## PNG
1. (16x16) px: ![](strl16.png)
2. (24x24) px: ![](strl24.png)
3. (32x32) px: ![](strl32.png)
4. (42x42) px: ![](strl42.png)
5. (64x64) px: ![](strl64.png)
6. (72x72) px: ![](strl72.png)
7. (128x128) px: ![](strl128.png)
8. (256x256) px: ![](strl256.png)
9. (512x512) px: ![](strl512.png)

# License

![CC](../cc.svg) ![BY](../by.svg) ![ND](../nd.svg)

All content of this site, except badges, are covered by Creative Commons CC-BY-ND license, because centralize information as standard.

## You are free to
- **share**: copy and redistribute the material in any medium or format
for any purpose, even commercially.

The licensor cannot revoke these freedoms as long as you follow the license terms.

## Under the following terms:

- **Attribution**: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

- **NoDerivatives**: If you remix, transform, or build upon the material, you may not distribute the modified material.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## Notice:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

See more in https://creativecommons.org/licenses/by-nd/4.0/



## Credits
2019-2022 (C) D. H. B. Marcos.

*Create in 2019.12.01 by D. H. B. Marcos.*

*Deo Ominis Gloria.*
