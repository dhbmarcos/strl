# Introduction

The primary purpose of using Simplest Technology Readiness Level is to help management in making decisions concerning the development and transitioning of technology. It should be viewed as one of several tools that are needed to manage the progress of research and development activity in your project, system or organization.

The Simplest Technology Readiness Level classifies the things in 3 levels:

1. ![undefined](undefined.svg)
2. ![unstable](unstable.svg)
3. ![stable](stable.svg)

|                   | ![undefined](undefined.svg)                | ![unstable](unstable.svg)              | ![stable](stable.svg)        |
| ----------------- | ------------------------------------------ | -------------------------------------- | ---------------------------- |
| **Modifications** | Things that are not completely defined.    | Things defined, but they can change.   | Stuffs that will not change. |
| **Marketing**     | Things before market or client validation. | Things in market or client validation. | Things validated.            |

